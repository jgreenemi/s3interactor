# README #

S3Interactor. Presently used for listing keys in a bucket (and subfolder if desired) to find all keys matching a specified string, then delete them. 

### Roadmap ###

* Implement a Scala version of the same code using the aws-scala package from Atlassian.
