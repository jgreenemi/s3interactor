import java.io.IOException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.*;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Interactor {
    private static String bucketName = "a-practice-bucket";
    private static String folderName = "bigdir/"; // If you want to include a prefix to match, type it in here with trailing / character.
    static String badstring = "¯"; //Set the string which, if present in a key's filename, will mark it for deletion.

    public static void main(String[] args) throws IOException {
        AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
        try {
            System.out.println("Listing objects for " + bucketName);
            final ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(folderName).withMaxKeys(1000); //There's a limit to how many keys can be listed at once.
            ListObjectsV2Result result;

            do {
                result = s3client.listObjectsV2(req);
                req.setContinuationToken(result.getNextContinuationToken());

                for (S3ObjectSummary objectSummary :
                        result.getObjectSummaries()) {

                    if (objectSummary.getKey().contains(badstring) == true) {
                        System.out.println(objectSummary.getKey() + " is being deleted for matching badstring [" + badstring + "] !");
                        s3client.deleteObject(bucketName, objectSummary.getKey());
                    } else {
                        System.out.println(objectSummary.getKey() + " was left unchanged!");
                    }
                }
            } while (result.isTruncated() == true);

        } catch (AmazonServiceException ase){
            System.out.println("AmazonServiceException: " + ase.getMessage() + ";" + ase.getStatusCode() + ";" + ase.getErrorCode());
        } catch (AmazonClientException ace){
            System.out.println("AmazonClientException: " + ace.getMessage());
        }
    }
}
