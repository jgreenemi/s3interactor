import com.amazonaws.services.s3.AmazonS3Client
import io.atlassian.aws.{AmazonClient, AmazonClientConnectionDef}

object S3InteractorScala {
  // I've a folder with many files in it, and I want to find and delete ones with a special character in their name.
  def main(args: Array[String]): Unit = {
    val defaultClient = AmazonClient[AmazonS3Client]
    val withEndpoint = AmazonClient[AmazonS3Client]
    val config = AmazonClientConnectionDef
  }
}
